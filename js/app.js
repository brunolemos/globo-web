$(document).ready(function() {
  $('.gif-item').click(function(e) {
    var href = $(e.currentTarget).data('gif-src');
    var articleUrl = $(e.currentTarget).data('article-url');

    $('#gif-main').attr('gif-src', href);
    $('#gif-main').data('article-url', articleUrl);
  });

  $('#gif-main').click(function(e) {
    var articleUrl = $(e.currentTarget).data('article-url');
    openArticle(articleUrl);
  });

  $(document).keypress(function(e) {
    if(e.keyCode !== 32) return;

    var articleUrl = $('#gif-main').data('article-url');
    openArticle(articleUrl);
  });
});

function openArticle(url) {
  if(!url) return;

  window.open(url, '_top');
}
